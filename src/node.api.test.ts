const mockedFavicons = jest.fn(() => ({
  render: mockedRenderFunction,
}));
const mockedRenderFunction = jest.fn(() => Promise.resolve('Arbitrary rendered output'));
jest.mock('@kuroku/react-static-favicons', () => mockedFavicons);

import nodeApi from './node.api';
const mockReactStaticConfig = {
  paths: {
    ASSETS: 'arbitrary assets path',
  },
};

beforeEach(() => {
  mockedFavicons.mockClear();
  mockedRenderFunction.mockClear();
});

it('should error out if no input file path was provided', async () => {
  const originalConsoleError = console.error;
  const mockConsoleError = jest.fn();
  console.error = mockConsoleError;

  expect(nodeApi({} as any)).toBeUndefined();
  expect(mockConsoleError.mock.calls.length).toBe(1);
  expect(mockConsoleError.mock.calls[0][0])
    .toBe('[react-static-plugin-favicons] Please specify a source image for your favicon through the `inputFile` parameter.');

  console.error = originalConsoleError;
});

it('should render the favicons', async () => {
  const plugin = nodeApi({
    inputFile: 'arbitrary-file',
  })!;

  mockedRenderFunction.mockReturnValueOnce(Promise.resolve('rendered favicon metadata'));

  plugin.config(mockReactStaticConfig);
  const mockHookOptions = { meta: {} as any };
  await plugin.beforeRenderToHtml('arbitrary element' as unknown as JSX.Element, mockHookOptions);

  expect(mockHookOptions.meta.faviconsElements).toEqual('rendered favicon metadata');
});

it('should add the rendered favicons to the `<head>`', () => {
  const plugin = nodeApi({
    inputFile: 'arbitrary-file',
  })!;

  expect(plugin.Head({ meta: { faviconsElements: 'rendered favicon metadata' as unknown as JSX.Element[] } }))
    .toBe('rendered favicon metadata');
});

it('should use config.paths.ASSETS as outputPath if none is provided', () => {
  const plugin = nodeApi({
    inputFile: 'arbitrary-file',
  })!;

  plugin.config({
    ...mockReactStaticConfig,
    paths: {
      ...mockReactStaticConfig.paths,
      ASSETS: 'some assets dir',
    },
  });
  expect(mockedFavicons.mock.calls.length).toBe(1);
  expect(mockedFavicons.mock.calls[0][0].outputDir).toBe('some assets dir');
});

it('should only initialise React Static Favicons once', () => {
  const plugin = nodeApi({
    inputFile: 'arbitrary-file',
  })!;

  plugin.config(mockReactStaticConfig);
  plugin.config(mockReactStaticConfig);
  expect(mockedFavicons.mock.calls.length).toBe(1);
});

// This test only works when ran in isolation, because the module uses a global variable in its
// module scope to keep track of multiple invocations.
it.skip('should only render the favicons once', async () => {
  const plugin = nodeApi({
    inputFile: 'arbitrary-file',
  })!;

  plugin.config(mockReactStaticConfig);
  const mockHookOptions = { meta: {} as any };
  plugin.beforeRenderToHtml('arbitrary element' as unknown as JSX.Element, mockHookOptions);
  plugin.beforeRenderToHtml('arbitrary element' as unknown as JSX.Element, mockHookOptions);

  expect(mockedRenderFunction.mock.calls.length).toBe(1);
});
